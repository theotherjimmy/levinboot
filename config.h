/* SPDX-License-Identifier: CC0-1.0 */
#pragma once

/* must be shorter than 64 bytes */
#define CONFIG_GREETING "levinboot/0.1\r\n"

/* base clock 1.5MHz */
#define CONFIG_UART_CLOCK_DIV 1
